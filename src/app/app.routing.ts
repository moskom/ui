import { NgModule }             from '@angular/core';
import { RouterModule, Routes, ExtraOptions } from '@angular/router';

// components
import { AppComponent } from './app.component';
import { LandingComponent } from './landing/landing.component';
import { PrintingComponent } from './printing/printing.component';
import { BscardComponent } from './bscard/bscard.component';
import { TshirtComponent } from './tshirt/tshirt.component';
import { LetterComponent } from './letter/letter.component';
import { QuickComponent } from './quick/quick.component';
import { StampComponent } from './stamp/stamp.component';
import { SealComponent } from './seal/seal.component';

const routes: Routes = [
  { 
    path: '',
    redirectTo: '/home',
    pathMatch: 'full' 
  },
  {
    path: 'home', 
    component: LandingComponent,
  },
  {
    path: 'printing', 
    component: PrintingComponent,
  },
  {
    path: 'business-card', 
    component: BscardComponent,
  },
  {
    path: 't-shirt', 
    component: TshirtComponent,
  },
  {
    path: 'letter', 
    component: LetterComponent,
  },
  {
    path: 'quick', 
    component: QuickComponent,
  },
  {
    path: 'quick/stamp', 
    component: StampComponent,
  },
  {
    path: 'quick/seal', 
    component: SealComponent,
  },
];

const routeOptions: ExtraOptions = {
  anchorScrolling: 'enabled',
  scrollPositionRestoration: 'enabled'
};

@NgModule({
  imports: [RouterModule.forRoot(routes, routeOptions)],
  exports: [RouterModule]
})
export class AppRoutingModule {}