import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data/data.service'
import { AppConst } from '../constants/app.const';
import { Router } from "@angular/router";
import { MatSnackBar } from '@angular/material';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent implements OnInit {

	contact: Contact = new Contact();

  constructor(public _dataService: DataService) { }

  ngOnInit() {
  }

  sendContact(){
  	this._dataService.sendContact(this.contact).subscribe(data=>{
  		this.contact = new Contact();
  	})
  }
}

export class Contact { }