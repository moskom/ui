import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-printing',
  templateUrl: './printing.component.html',
  styleUrls: ['./printing.component.css']
})
export class PrintingComponent implements OnInit {

	products = [
		{'name': 'Business Cards', 'url': 'business-card', 'image': 'card.jpeg'},
		// {'name': 'T-shirt', 'url': 't-shirt', 'image': 'tshirt.jpeg'},
		// {'name': 'Letterheads', 'url': 'letter', 'image': 'letter.jpeg'},
		// {'name': 'Envelopes', 'url': 'url', 'image': 'envelope.jpeg'},
		// {'name': 'Continuous Stationery', 'url': 'url', 'image': 'stationary.jpeg'},
		// {'name': 'Folders', 'url': 'url', 'image': 'folder.jpg'},
	]

  constructor() { }

  ngOnInit() {
  }

}
