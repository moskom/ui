import { Router } from '@angular/router';
import { AfterViewInit, OnInit, Component, HostBinding, Output, EventEmitter } from '@angular/core';
import {
  animate,
  state,
  style,
  transition,
  trigger
} from '@angular/animations';
import { fromEvent } from 'rxjs';
import {
  distinctUntilChanged,
  filter,
  map,
  pairwise,
  share,
  throttleTime
} from 'rxjs/operators';

enum VisibilityState {
  Visible = 'visible',
  Hidden = 'hidden'
}

enum Direction {
  Up = 'Up',
  Down = 'Down'
}

@Component({
  selector: 'app-sticky-header',
  templateUrl: 'sticky-header.component.html',
  // template: `<ng-content></ng-content>`,
  styleUrls: ['sticky-header.component.css'],
  animations: [
    trigger('toggle', [
      state(
        VisibilityState.Hidden,
        style({ opacity: 0, transform: 'translateY(-100%)' })
      ),
      state(
        VisibilityState.Visible,
        style({ opacity: 1, transform: 'translateY(0)' })
      ),
      transition('* => *', animate('200ms ease-in'))
    ])
  ]
})
export class StickyHeaderComponent implements OnInit, AfterViewInit {
  private isVisible = true;
  @Output() public sidenavToggle = new EventEmitter();

  @HostBinding('@toggle')
  get toggle(): VisibilityState {
    return this.isVisible ? VisibilityState.Visible : VisibilityState.Hidden;
  }

  urlname: any;
  list = [];
  homeNav: boolean = false;
  printingNav: boolean = false;

  constructor(private route: Router) { }

  ngOnInit() {
    this.route.events.subscribe((event: any) => {
      this.urlEvent(event.url);
    });
  }

  urlEvent(url) {
    this.list.push(url);
    this.urlname = this.list[0];
    if(this.urlname.includes('/home') || this.urlname == '/'){
      this.homeNav = true;
      this.printingNav = false;
    }else if(this.urlname.includes('/printing') || this.urlname.includes('/business-card')){
      this.printingNav = true;
      this.homeNav = false;
    }
  }

  ngAfterViewInit() {
    const scroll$ = fromEvent(window, 'scroll').pipe(
      throttleTime(10),
      map(() => window.pageYOffset),
      pairwise(),
      map(([y1, y2]): Direction => (y2 < y1 ? Direction.Up : Direction.Down)),
      distinctUntilChanged(),
      share()
    );

    const goingUp$ = scroll$.pipe(
      filter(direction => direction === Direction.Up)
    );

    const goingDown$ = scroll$.pipe(
      filter(direction => direction === Direction.Down)
    );

    goingUp$.subscribe(() => (this.isVisible = true));
    goingDown$.subscribe(() => (this.isVisible = false));
  }

  public onToggleSidenav = () => {
    this.sidenavToggle.emit();
  }

}
