import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BscardComponent } from './bscard.component';

describe('BscardComponent', () => {
  let component: BscardComponent;
  let fixture: ComponentFixture<BscardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BscardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BscardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
