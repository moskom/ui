import { Component, OnInit, ViewChild, ElementRef, Inject } from '@angular/core';
import { FormBuilder, FormGroup } from "@angular/forms";

@Component({
  selector: 'app-bscard',
  templateUrl: './bscard.component.html',
  styleUrls: ['./bscard.component.css']
})
export class BscardComponent implements OnInit {
  @ViewChild('fileInput') fileInput: ElementRef;
  form: FormGroup;
  uploadingform: boolean = false;
  msg: any;
  campaignleads: any;
  noFile: boolean = true;

  error: boolean = false;
  success: boolean = false;

  def_card: DefCard = new DefCard();

  constructor(
    private fb: FormBuilder,
  ) {
  	this.createForm(); 
  }

  ngOnInit() {
  }

  createForm(){
    this.form = this.fb.group({
      file: null
    });
  }
  onFileChange(event) {
    this.success = false;
    this.error = false;
    if(event.target.files.length > 0) {
      let file = event.target.files[0];
      this.form.get('file').setValue(file);
    }
    if(event.target.files.length < 1){
      this.noFile = true;
    }else{
      this.noFile = false;
    }
  }
  private prepareSave(): any {
    let input = new FormData();
    this.noFile = false;
    return input;
  }
  clearFile(){
    this.form.get('file').setValue(null);
    this.fileInput.nativeElement.value = '';
    this.noFile = true;
  }
  onSubmit() {
    this.uploadingform = true;
    const formModel = this.prepareSave();
  }

}

export class DefCard{}