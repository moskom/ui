import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { AppConst } from '../../constants/app.const';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private serverURL:string = AppConst.serverPath;

  constructor(private http: HttpClient) { }

  sendContact(msg){
    let url = this.serverURL+'contact/';
    return this.http.post(url, msg, httpOptions) 
  }
}
