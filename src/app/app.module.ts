import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { StickyHeaderComponent } from './sticky-header/sticky-header.component';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MaterialModule } from './material/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';

//routing module
import { AppRoutingModule } from './app.routing';
import { ScrollToModule } from 'ng2-scroll-to';

// components
import { LandingComponent } from './landing/landing.component';
import { LayoutComponent } from './layout/layout.component';
import { SidenavListComponent } from './sidenav-list/sidenav-list.component';
import { PrintingComponent } from './printing/printing.component';
import { BscardComponent } from './bscard/bscard.component';
import { FooterComponent } from './footer/footer.component';
import { PrintHeaderComponent } from './print-header/print-header.component';
import { TshirtComponent } from './tshirt/tshirt.component';
import { LetterComponent } from './letter/letter.component';
import { QuickComponent } from './quick/quick.component';
import { StampComponent } from './stamp/stamp.component';
import { SealComponent } from './seal/seal.component';

import { FacebookModule } from 'ngx-facebook';

@NgModule({
  declarations: [
    AppComponent, 
    StickyHeaderComponent, 
    LandingComponent,
    LayoutComponent,
    SidenavListComponent,
    PrintingComponent,
    BscardComponent,
    FooterComponent,
    PrintHeaderComponent,
    TshirtComponent,
    LetterComponent,
    QuickComponent,
    StampComponent,
    SealComponent,
  ],
  imports: [
  	BrowserModule, 
  	BrowserAnimationsModule,
    AppRoutingModule,
    MaterialModule,
    FlexLayoutModule,
    HttpModule,
    HttpClientModule,
    FormsModule, 
    ReactiveFormsModule,
    ScrollToModule,
    FacebookModule.forRoot()
  ],
  providers: [
    { 
      provide: LocationStrategy, 
      useClass: HashLocationStrategy 
    },
  ],
  bootstrap: [AppComponent],
  entryComponents: [ ]
})
export class AppModule {}
