import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { 
  MatTabsModule,
  MatSidenavModule,
  MatListModule,
  MatFormFieldModule, 
  MatInputModule,
  MatCardModule,
  MatButtonModule,
  MatToolbarModule,
  MatIconModule,
  MatChipsModule,
  MatTableModule,
  MatPaginatorModule,
  MatMenuModule,
  MatSnackBarModule,
  MatProgressSpinnerModule,
  MatSelectModule,
  MatRadioModule,
} from '@angular/material';

@NgModule({
  imports: [
    MatTabsModule,
    MatSidenavModule,
    MatListModule,
    MatFormFieldModule, 
    MatInputModule,
    MatCardModule,
    MatButtonModule,
    MatToolbarModule,
    MatIconModule,
    MatChipsModule,
    MatTableModule,
    MatPaginatorModule,
    MatMenuModule,
    MatSnackBarModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    MatRadioModule,
  ],
  exports: [
    MatTabsModule,
    MatSidenavModule,
    MatListModule,
    MatFormFieldModule, 
    MatInputModule,
    MatCardModule,
    MatButtonModule,
    MatToolbarModule,
    MatIconModule,
    MatChipsModule,
    MatTableModule,
    MatPaginatorModule,
    MatMenuModule,
    MatSnackBarModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    MatRadioModule,
  ],
  declarations: []
})
export class MaterialModule { }
