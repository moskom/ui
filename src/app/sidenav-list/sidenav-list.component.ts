import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sidenav-list',
  templateUrl: './sidenav-list.component.html',
  styleUrls: ['./sidenav-list.component.css']
})
export class SidenavListComponent implements OnInit {
  @Output() sidenavClose = new EventEmitter();

  urlname: any;
  list = [];
  homeNav: boolean = false;
  printingNav: boolean = false;

  constructor(private route: Router) { }

  ngOnInit() {
    this.route.events.subscribe((event: any) => {
      this.urlEvent(event.url);
    });
  }

  urlEvent(url) {
    this.list.push(url);
    this.urlname = this.list[0];
    if(this.urlname.includes('/home') || this.urlname == '/'){
      this.homeNav = true;
      this.printingNav = false;
    }else if(this.urlname.includes('/printing') || this.urlname.includes('/business-card')){
      this.printingNav = true;
      this.homeNav = false;
    }
  }

  public onSidenavClose = () => {
    this.sidenavClose.emit();
  }
}
